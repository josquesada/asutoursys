export default {
  appendContactInfo: (state, obj) => {
    state.contactInfoData.push(obj)
  },
  updateContactInfo: (state, obj) => {
    state.contactInfoData[obj.pos] = obj.data
  },
  deleteDataFromArray: (state, obj) => {
    if (obj.removeAll) {
      state[obj.storage] = []
    } else {
      obj.dataToRemoved.forEach(element => {
        state[obj.storage].splice(element, 1)
      })
    }
  }
}

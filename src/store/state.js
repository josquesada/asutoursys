import quotes from '../data/quotes'
import dataTemplates from '../data/templates'
import emailTemplates from '../data/emails'
import servicesList from '../data/services'
import providersList from '../data/providers'
import contactInfoData from '../data/contactInfoData'

export default {
  quotes,
  dataTemplates,
  emailTemplates,
  servicesList,
  providersList,
  contactInfoData
}

export default {
  getQuotes: (state) => {
    return state.quotes
  },
  getQuotesByStatus: (state, _status) => {
    return state.quotes.estado === _status
  }
}

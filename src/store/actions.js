export default {
  addContactInfo: ({ commit }, obj) => {
    commit('appendContactInfo', obj)
  },
  updateContactInfo: ({ commit }, obj) => {
    commit('updateContactInfo', obj)
  },
  deleteDataFromArray: ({ commit }, obj) => {
    commit('deleteDataFromArray', obj)
  }
}

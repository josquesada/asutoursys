export default {
  sumary: [
    {
      count: 643,
      desc: 'Proveedores activos'
    },
    {
      count: 1256,
      desc: 'Servicios disponibles'
    },
    {
      count: 25,
      desc: 'Ciudades abarcadas'
    },
    {
      count: 40,
      desc: 'Agregados recientemente'
    }
  ],
  providers: [
    {
      id: 1,
      code: 'ACSJD1',
      category_id: 1,
      category_name: 'Alojamiento',
      name: 'Hotel holiday INN',
      city: 'San Jose',
      services: 6,
      comments: 'Cerca a la alcaldia y museo'
    },
    {
      id: 2,
      code: 'ACSJD2',
      category_id: 2,
      category_name: 'Alquiler de coches',
      name: 'Thrifty hoteles',
      city: 'San Jose',
      services: 2,
      comments: 'Cerca a la alcaldia y museo'
    },
    {
      id: 3,
      code: 'ACSJD3',
      category_id: 1,
      category_name: 'Alojamiento',
      name: 'Hotel holiday INN',
      city: 'San Jose',
      services: 8,
      comments: 'Cerca a la alcaldia y museo'
    },
    {
      id: 4,
      code: 'ACSJD4',
      category_id: 2,
      category_name: 'Alquiler de coches',
      name: 'Thrifty hoteles',
      city: 'San Jose',
      services: 2,
      comments: 'Cerca a la alcaldia y museo'
    },
    {
      id: 5,
      code: 'ACSJD5',
      category_id: 1,
      category_name: 'Alojamiento',
      name: 'La pelusa parador turistico',
      city: 'San Jose',
      services: 6,
      comments: 'Cerca a la alcaldia y museo'
    },
    {
      id: 6,
      code: 'ACSJD6',
      category_id: 1,
      category_name: 'Alojamiento',
      name: 'El ojo posadas',
      city: 'San Jose',
      services: 6,
      comments: 'Cerca a la alcaldia y museo'
    }
  ]
}

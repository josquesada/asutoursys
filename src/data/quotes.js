export default [
  {
    booking_id: 1,
    booking_owner: 12,
    booking_number: 'BKFT141468',
    booking_name: 'Viaje Familiar para 2 personas',
    booking_category: 'Familiar',
    booking_budget: '6500 USD',
    agent_id: 223,
    agent: 'Fernando Soto',
    client: 'Juan Gabriel',
    language: 'Español',
    country: 'Costa Rica',
    cities: 5,
    travel_date: '20/10/19',
    travel_days: 15,
    created_at: '20/10/19',
    status_text: 'En reserva',
    status_class: 'En reserva',
    status: 1,
    pax: {
      adults: 2,
      children: 1,
      infants: 0,
      rooms: [
        {
          type: 'Single bed',
          name: 'Hab 1',
          detail: [
            {
              name: 'Josue',
              last_name: 'Quesada Parajeles',
              age: 28,
              country: 'Costa Rica',
              dni: 114540644,
              cost_person: '6500 USD'
            },
            {
              name: 'Daniela',
              last_name: 'Carvajal',
              age: 26,
              country: 'Costa Rica',
              dni: 15682882,
              cost_person: '6500 USD'
            }
          ]
        },
        {
          type: 'Double Room',
          name: 'Hab 2',
          detail: [
            {
              name: 'Jairo',
              last_name: 'Mendez',
              age: 29,
              country: 'Costa Rica',
              dni: 100540644,
              cost_person: '8500 USD'
            },
            {
              name: 'Yuli',
              last_name: 'Chinchilla',
              age: 26,
              country: 'Costa Rica',
              dni: 35682882,
              cost_person: '6500 USD'
            },
            {
              name: 'Cristi',
              last_name: 'Amores',
              age: 28,
              country: 'Costa Rica',
              dni: 85682882,
              cost_person: '7500 USD'
            }
          ]
        }

      ]
    },
    itinerary: {
      routes: [
        {
          title: '',
          place: 'Ciudad: Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        }
      ],
      services: [
        {
          title: '21 Nov 2019',
          detail: [
            {
              code: 'ACLBHI',
              location: 'Liberia',
              start_date: '20-10-19',
              end_date: '20-10-19',
              provider: 'Holiday Inn',
              alt: '1',
              price: '897 USD',
              percentage: '20%',
              status_text: 'Description',
              status: 1
            },
            {
              code: 'ACLBHI',
              location: 'Liberia',
              start_date: '20-10-19',
              end_date: '20-10-19',
              provider: 'Holiday Inn',
              alt: '1',
              price: '897 USD',
              percentage: '20%',
              status_text: 'Description',
              status: 1
            },
            {
              code: 'ACLBHI',
              location: 'Liberia',
              start_date: '20-10-19',
              end_date: '20-10-19',
              provider: 'Holiday Inn',
              alt: '1',
              price: '897 USD',
              percentage: '20%',
              status_text: 'Description',
              status: 1
            },
            {
              code: 'ACLBHI',
              location: 'Liberia',
              start_date: '20-10-19',
              end_date: '20-10-19',
              provider: 'Holiday Inn',
              alt: '1',
              price: '897 USD',
              percentage: '20%',
              status_text: 'Description',
              status: 1
            },
            {
              code: 'ACLBHI',
              location: 'Liberia',
              start_date: '20-10-19',
              end_date: '20-10-19',
              provider: 'Holiday Inn',
              alt: '1',
              price: '897 USD',
              percentage: '20%',
              status_text: 'Description',
              status: 1
            },
            {
              code: 'ACLBHI',
              location: 'Liberia',
              start_date: '20-10-19',
              end_date: '20-10-19',
              provider: 'Holiday Inn',
              alt: '1',
              price: '897 USD',
              percentage: '20%',
              status_text: 'Description',
              status: 1
            }
          ],
          tags: [
            {
              name: 'Liberia'
            },
            {
              name: 'Guanacaste'
            }
          ]
        },
        {
          title: '21 Nov 2019',
          detail: [
            {
              code: 'ACLBHI',
              location: 'Liberia',
              start_date: '20-10-19',
              end_date: '20-10-19',
              provider: 'Holiday Inn',
              alt: '1',
              price: '897 USD',
              percentage: '20%',
              status_text: 'Description',
              status: 1
            },
            {
              code: 'ACLBHI',
              location: 'Liberia',
              start_date: '20-10-19',
              end_date: '20-10-19',
              provider: 'Holiday Inn',
              alt: '1',
              price: '897 USD',
              percentage: '20%',
              status_text: 'Description',
              status: 1
            },
            {
              code: 'ACLBHI',
              location: 'Liberia',
              start_date: '20-10-19',
              end_date: '20-10-19',
              provider: 'Holiday Inn',
              alt: '1',
              price: '897 USD',
              percentage: '20%',
              status_text: 'Description',
              status: 1
            },
            {
              code: 'ACLBHI',
              location: 'Liberia',
              start_date: '20-10-19',
              end_date: '20-10-19',
              provider: 'Holiday Inn',
              alt: '1',
              price: '897 USD',
              percentage: '20%',
              status_text: 'Description',
              status: 1
            },
            {
              code: 'ACLBHI',
              location: 'Liberia',
              start_date: '20-10-19',
              end_date: '20-10-19',
              provider: 'Holiday Inn',
              alt: '1',
              price: '897 USD',
              percentage: '20%',
              status_text: 'Description',
              status: 1
            },
            {
              code: 'ACLBHI',
              location: 'Liberia',
              start_date: '20-10-19',
              end_date: '20-10-19',
              provider: 'Holiday Inn',
              alt: '1',
              price: '897 USD',
              percentage: '20%',
              status_text: 'Description',
              status: 1
            }
          ],
          tags: [
            {
              name: 'Liberia'
            },
            {
              name: 'Guanacaste'
            }
          ]
        },
        {
          title: '21 Nov 2019',
          detail: [
            {
              code: 'ACLBHI',
              location: 'Liberia',
              start_date: '20-10-19',
              end_date: '20-10-19',
              provider: 'Holiday Inn',
              alt: '1',
              price: '897 USD',
              percentage: '20%',
              status_text: 'Description',
              status: 1
            },
            {
              code: 'ACLBHI',
              location: 'Liberia',
              start_date: '20-10-19',
              end_date: '20-10-19',
              provider: 'Holiday Inn',
              alt: '1',
              price: '897 USD',
              percentage: '20%',
              status_text: 'Description',
              status: 1
            },
            {
              code: 'ACLBHI',
              location: 'Liberia',
              start_date: '20-10-19',
              end_date: '20-10-19',
              provider: 'Holiday Inn',
              alt: '1',
              price: '897 USD',
              percentage: '20%',
              status_text: 'Description',
              status: 1
            },
            {
              code: 'ACLBHI',
              location: 'Liberia',
              start_date: '20-10-19',
              end_date: '20-10-19',
              provider: 'Holiday Inn',
              alt: '1',
              price: '897 USD',
              percentage: '20%',
              status_text: 'Description',
              status: 1
            },
            {
              code: 'ACLBHI',
              location: 'Liberia',
              start_date: '20-10-19',
              end_date: '20-10-19',
              provider: 'Holiday Inn',
              alt: '1',
              price: '897 USD',
              percentage: '20%',
              status_text: 'Description',
              status: 1
            },
            {
              code: 'ACLBHI',
              location: 'Liberia',
              start_date: '20-10-19',
              end_date: '20-10-19',
              provider: 'Holiday Inn',
              alt: '1',
              price: '897 USD',
              percentage: '20%',
              status_text: 'Description',
              status: 1
            }
          ],
          tags: [
            {
              name: 'Liberia'
            },
            {
              name: 'Guanacaste'
            }
          ]
        },
        {
          title: '21 Nov 2019',
          detail: [
            {
              code: 'ACLBHI',
              location: 'Liberia',
              start_date: '20-10-19',
              end_date: '20-10-19',
              provider: 'Holiday Inn',
              alt: '1',
              price: '897 USD',
              percentage: '20%',
              status_text: 'Description',
              status: 1
            },
            {
              code: 'ACLBHI',
              location: 'Liberia',
              start_date: '20-10-19',
              end_date: '20-10-19',
              provider: 'Holiday Inn',
              alt: '1',
              price: '897 USD',
              percentage: '20%',
              status_text: 'Description',
              status: 1
            },
            {
              code: 'ACLBHI',
              location: 'Liberia',
              start_date: '20-10-19',
              end_date: '20-10-19',
              provider: 'Holiday Inn',
              alt: '1',
              price: '897 USD',
              percentage: '20%',
              status_text: 'Description',
              status: 1
            },
            {
              code: 'ACLBHI',
              location: 'Liberia',
              start_date: '20-10-19',
              end_date: '20-10-19',
              provider: 'Holiday Inn',
              alt: '1',
              price: '897 USD',
              percentage: '20%',
              status_text: 'Description',
              status: 1
            },
            {
              code: 'ACLBHI',
              location: 'Liberia',
              start_date: '20-10-19',
              end_date: '20-10-19',
              provider: 'Holiday Inn',
              alt: '1',
              price: '897 USD',
              percentage: '20%',
              status_text: 'Description',
              status: 1
            },
            {
              code: 'ACLBHI',
              location: 'Liberia',
              start_date: '20-10-19',
              end_date: '20-10-19',
              provider: 'Holiday Inn',
              alt: '1',
              price: '897 USD',
              percentage: '20%',
              status_text: 'Description',
              status: 1
            }
          ],
          tags: [
            {
              name: 'Liberia'
            },
            {
              name: 'Guanacaste'
            }
          ]
        }
      ]
    }
  },
  {
    booking_id: 2,
    booking_owner: 12,
    booking_number: 'BKFT141468',
    booking_name: 'Viaje Familiar para 3 personas',
    booking_category: 'Familiar',
    booking_budget: '8500 USD',
    agent_id: 223,
    agent: 'Fernando Soto',
    client: 'Luis Marin',
    language: 'Español',
    country: 'Costa Rica',
    cities: 5,
    travel_date: '28/10/19',
    travel_days: 15,
    created_at: '28/10/19',
    status_text: 'En reserva',
    status: 1,
    itinerary: {
      routes: [
        {
          title: 'Viaje Familiar para 2 personas',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        }
      ]
    }
  },
  {
    booking_id: 3,
    booking_owner: 12,
    booking_number: 'BKFT141468',
    booking_name: 'Viaje Familiar para 5 personas',
    booking_category: 'Familiar',
    booking_budget: '16800 USD',
    agent_id: 223,
    agent: 'Fernando Soto',
    client: 'Cristina Calvo',
    language: 'Español',
    country: 'Costa Rica',
    cities: 5,
    travel_date: '05/10/19',
    travel_days: 15,
    created_at: '05/10/19',
    status_text: 'Por Aprobar',
    status: 2,
    itinerary: {
      routes: [
        {
          title: 'Viaje Familiar para 2 personas',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        }
      ]
    }
  },
  {
    booking_id: 4,
    booking_owner: 12,
    booking_number: 'BKFT141468',
    booking_name: 'Viaje Familiar para 12 personas',
    booking_category: 'Familiar',
    booking_budget: '22500 USD',
    agent_id: 223,
    agent: 'Fernando Soto',
    client: 'Adrian Fallas',
    language: 'Español',
    country: 'Costa Rica',
    cities: 5,
    travel_date: '10/10/19',
    travel_days: 20,
    created_at: '10/10/19',
    status_text: 'Por Aprobar',
    status: 2,
    itinerary: {
      routes: [
        {
          title: 'Viaje Familiar para 2 personas',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        }
      ]
    }
  },
  {
    booking_id: 5,
    booking_owner: 12,
    booking_number: 'BKFT141468',
    booking_name: 'Viaje Familiar para 2 personas',
    booking_category: 'Familiar',
    booking_budget: '6500 USD',
    agent_id: 223,
    agent: 'Fernando Soto',
    client: 'Josue Quesada',
    language: 'Español',
    country: 'Costa Rica',
    cities: 5,
    travel_date: '20/10/19',
    travel_days: 15,
    created_at: '20/10/19',
    status_text: 'Por Aprobar',
    status: 2,
    itinerary: {
      routes: [
        {
          title: 'Viaje Familiar para 2 personas',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        }
      ]
    }
  },
  {
    booking_id: 6,
    booking_owner: 12,
    booking_number: 'BKFT141468',
    booking_name: 'Viaje Familiar para 2 personas',
    booking_category: 'Familiar',
    booking_budget: '6500 USD',
    agent_id: 223,
    agent: 'Fernando Soto',
    client: 'Jairo Mendez',
    language: 'Español',
    country: 'Costa Rica',
    cities: 5,
    travel_date: '25/10/19',
    travel_days: 15,
    created_at: '25/10/19',
    status_text: 'Cambios Web',
    status: 3,
    itinerary: {
      routes: [
        {
          title: 'Viaje Familiar para 2 personas',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        }
      ]
    }
  },
  {
    booking_id: 7,
    booking_owner: 12,
    booking_number: 'BKFT141468',
    booking_name: 'Viaje Familiar para 2 personas',
    booking_category: 'Familiar',
    booking_budget: '6500 USD',
    agent_id: 223,
    agent: 'Fernando Soto',
    client: 'Maria Fonsenca',
    language: 'Español',
    country: 'Costa Rica',
    cities: 5,
    travel_date: '23/10/19',
    travel_days: 15,
    created_at: '23/10/19',
    status_text: 'Cambios Web',
    status: 3,
    itinerary: {
      routes: [
        {
          title: 'Viaje Familiar para 2 personas',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        }
      ]
    }
  },
  {
    booking_id: 8,
    booking_owner: 12,
    booking_number: 'BKFT141468',
    booking_name: 'Viaje Familiar para 2 personas',
    booking_category: 'Familiar',
    booking_budget: '6500 USD',
    agent_id: 223,
    agent: 'Fernando Soto',
    client: 'Gabriela Cespedes',
    language: 'Español',
    country: 'Costa Rica',
    cities: 5,
    travel_date: '20/10/19',
    travel_days: 15,
    created_at: '20/10/19',
    status_text: 'Cambios Web',
    status: 3,
    itinerary: {
      routes: [
        {
          title: 'Viaje Familiar para 2 personas',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        }
      ]
    }
  },
  {
    booking_id: 9,
    booking_owner: 12,
    booking_number: 'BKFT141468',
    booking_name: 'Viaje Familiar para 2 personas',
    booking_category: 'Familiar',
    booking_budget: '6500 USD',
    agent_id: 223,
    agent: 'Fernando Soto',
    client: 'Luis Barrantes',
    language: 'Español',
    country: 'Costa Rica',
    cities: 5,
    travel_date: '20/10/19',
    travel_days: 15,
    created_at: '20/10/19',
    status_text: 'Para Pago',
    status: 4,
    itinerary: {
      routes: [
        {
          title: 'Viaje Familiar para 2 personas',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        }
      ]
    }
  },
  {
    booking_id: 10,
    booking_owner: 12,
    booking_number: 'BKFT141468',
    booking_name: 'Viaje Familiar para 2 personas',
    booking_category: 'Familiar',
    booking_budget: '6500 USD',
    agent_id: 223,
    agent: 'Fernando Soto',
    client: 'Daniela Carvajal',
    language: 'Español',
    country: 'Costa Rica',
    cities: 5,
    travel_date: '20/10/19',
    travel_days: 15,
    created_at: '20/10/19',
    status_text: 'Para Pago',
    status: 4,
    itinerary: {
      routes: [
        {
          title: 'Viaje Familiar para 2 personas',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        },
        {
          title: '',
          place: 'Tortuguero',
          location: '123,456',
          description: 'Tortuguero, Costa Rica, which can be translated as Land of Turtles, is a village on the Northern Caribbean coast of Costa Rica in the Limón Province',
          web_changes: 'No hubo anotaciones esta vez.',
          services: [
            {
              type: 'change',
              description: 'Transporte desde el aeropuerto'
            },
            {
              type: 'change',
              description: '3 noches de hotel'
            },
            {
              type: '',
              description: 'Asistencia de viaje'
            },
            {
              type: 'change',
              description: 'Alimentation en el centro'
            },
            {
              type: '',
              description: 'Botones'
            }
          ]
        }
      ]
    }
  }
]

export default [
  {
    tabName: 'Recientes',
    templates: [
      {
        id: 1,
        code: 'ASDFG0',
        title: 'Aventura Familiar en Nicaragua',
        type_color: 1,
        type: 'Romantico',
        trip_cost: '4500 USD',
        days: 10,
        services: 20,
        src: 'image-1.jpg',
        destinations: [
          'San Jose',
          'Liberia',
          'Guanacaste',
          'Puntarenas',
          'Monteverde'
        ],
        comments: 'Lorem ipsum dolor sit amet..'
      },
      {
        id: 2,
        code: 'ASDFG1',
        title: 'Aventura Familiar en Costa Rica',
        type_color: 2,
        type: 'Familiar',
        trip_cost: '6500 USD',
        days: 12,
        services: 24,
        src: 'image-2.jpg',
        destinations: [
          'San Jose',
          'Liberia',
          'Guanacaste',
          'Tortuguero'
        ],
        comments: 'Lorem ipsum dolor sit amet..'
      },
      {
        id: 3,
        code: 'ASDFG3',
        title: 'Aventura Familiar en Guatemala',
        type_color: 3,
        type: 'Aventura',
        trip_cost: '3500 USD',
        days: 10,
        services: 20,
        src: 'image-1.jpg',
        destinations: [
          'San Jose',
          'Liberia',
          'Guanacaste',
          'Monteverde',
          'Tortuguero'
        ],
        comments: 'Lorem ipsum dolor sit amet..'
      }
    ]
  },
  {
    tabName: 'Mis Plantillas',
    templates: [
      {
        id: 4,
        code: 'ASDFG4',
        title: 'Aventura Familiar en Panama',
        type_color: 1,
        type: 'Urbano',
        trip_cost: '500 USD',
        days: 10,
        services: 20,
        src: 'image-1.jpg',
        destinations: [
          'San Jose',
          'Liberia',
          'Guanacaste',
          'Puntarenas',
          'Tortuguero'
        ],
        comments: 'Lorem ipsum dolor sit amet..'
      },
      {
        id: 5,
        code: 'ASDFG5',
        title: 'Aventura Familiar en Costa Rica',
        type_color: 2,
        type: 'Romantico',
        trip_cost: '8500 USD',
        days: 12,
        services: 24,
        src: 'image-2.jpg',
        destinations: [
          'San Jose',
          'Liberia',
          'Guanacaste',
          'Tortuguero'
        ],
        comments: 'Lorem ipsum dolor sit amet..'
      },
      {
        id: 6,
        code: 'ASDFG6',
        title: 'Aventura Novios  en Costa Rica',
        type_color: 3,
        type: 'Familar',
        trip_cost: '12500 USD',
        days: 10,
        services: 20,
        src: 'image-1.jpg',
        destinations: [
          'San Jose',
          'Liberia',
          'Guanacaste',
          'Monteverde',
          'Tortuguero'
        ],
        comments: 'Lorem ipsum dolor sit amet..'
      }
    ]
  }
]

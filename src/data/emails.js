export default [
  {
    tabName: 'Generales',
    templates: [
      {
        id: 1,
        code: 'MSJS01',
        title: 'Mensaje estandar 1',
        language: 'Español',
        comments: 'Para clientes de hispanohablantes'
      },
      {
        id: 2,
        code: 'MSJS02',
        title: 'Mensaje estandar 2',
        language: 'Español',
        comments: 'Para clientes de hispanohablantes'
      },
      {
        id: 3,
        code: 'MSJS03',
        title: 'Mensaje estandar 3',
        language: 'Frances',
        comments: 'Para clientes de hispanohablantes'
      }
    ]
  },
  {
    tabName: 'Mis Plantillas',
    templates: [
      {
        id: 4,
        code: 'MSJS04',
        title: 'Mensaje estandar 4',
        language: 'Español',
        comments: 'Para clientes de hispanohablantes'
      },
      {
        id: 5,
        code: 'MSJS05',
        title: 'Mensaje estandar 5',
        language: 'Aleman',
        comments: 'Para clientes de hispanohablantes'
      },
      {
        id: 6,
        code: 'MSJS06',
        title: 'Mensaje estandar 6',
        language: 'Ingles',
        comments: 'Para clientes de hispanohablantes'
      },
      {
        id: 7,
        code: 'MSJS07',
        title: 'Mensaje estandar 7',
        language: 'Español',
        comments: 'Para clientes de hispanohablantes'
      }
    ]
  }
]

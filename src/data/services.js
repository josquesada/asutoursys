export default [
  {
    id: 1,
    code: 'QWERTY',
    category_id: 12,
    category_name: 'Alojamiento',
    name: 'Habitacion doble',
    provider_id: 123,
    provider_name: 'Holiday inn',
    city: 'San Jose',
    price: 120,
    percentage: 12.6,
    has_promo: false,
    conditions: [],
    suggestions: [
      {
        id: 2,
        code: 'ASDFGH',
        category_id: 12,
        category_name: 'Alojamiento',
        name: 'Habitacion lujo',
        provider_id: 123,
        provider_name: 'Holiday inn',
        city: 'San Jose',
        price: 59,
        percentage: 30.6
      },
      {
        id: 3,
        code: 'POIUYT',
        category_id: 12,
        category_name: 'Alojamiento',
        name: 'Habitacion lujo',
        provider_id: 123,
        provider_name: 'Holiday inn',
        city: 'San Jose',
        price: 35,
        percentage: 30.6
      },
      {
        id: 4,
        code: 'MNBVCA',
        category_id: 12,
        category_name: 'Alojamiento',
        name: 'Habitacion lujo',
        provider_id: 123,
        provider_name: 'Holiday inn',
        city: 'San Jose',
        price: 50,
        percentage: 30.6
      }
    ],
    gallery: [],
    information: {
      description: '',
      features: [{}],
      incluides: [{}]
    }
  },
  {
    id: 2,
    code: 'ASDFGH',
    category_id: 12,
    category_name: 'Alojamiento',
    name: 'Habitacion lujo',
    provider_id: 123,
    provider_name: 'Holiday inn',
    city: 'San Jose',
    price: 59,
    percentage: 30.6,
    has_promo: true,
    conditions: [],
    suggestions: [],
    gallery: [],
    information: {
      description: '',
      features: [{}],
      incluides: [{}]
    }
  },
  {
    id: 3,
    code: 'POIUYT',
    category_id: 12,
    category_name: 'Alojamiento',
    name: 'Habitacion lujo',
    provider_id: 123,
    provider_name: 'Holiday inn',
    city: 'San Jose',
    price: 35,
    percentage: 30.6,
    has_promo: false,
    conditions: [],
    suggestions: [],
    gallery: [],
    information: {
      description: '',
      features: [{}],
      incluides: [{}]
    }
  },
  {
    id: 4,
    code: 'MNBVCA',
    category_id: 12,
    category_name: 'Alojamiento',
    name: 'Habitacion lujo',
    provider_id: 123,
    provider_name: 'Holiday inn',
    city: 'San Jose',
    price: 50,
    percentage: 30.6,
    has_promo: true,
    conditions: [],
    suggestions: [],
    gallery: [],
    information: {
      description: '',
      features: [{}],
      incluides: [{}]
    }
  },
  {
    id: 5,
    code: 'LPJSVY',
    category_id: 12,
    category_name: 'Alojamiento',
    name: 'Habitacion lujo',
    provider_id: 123,
    provider_name: 'Holiday inn',
    city: 'San Jose',
    price: 80,
    percentage: 30.6,
    has_promo: true,
    conditions: [],
    suggestions: [],
    gallery: [],
    information: {
      description: '',
      features: [{}],
      incluides: [{}]
    }
  }
]

import Vue from 'vue'

import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import './assets/stylus/index.styl'

import App from './App.vue'
import router from './router'
import store from './store'

import VueFeather from 'vue-feather'
import VueColumnsResizable from 'vue-columns-resizable'
import VueTelInput from 'vue-tel-input'
Vue.use(VueTelInput)

Vue.use(VueColumnsResizable)
Vue.use(VueFeather)

// BootstrapVue
Vue.use(BootstrapVue)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

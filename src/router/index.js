import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

// Quotes
import Quotes from '../views/Quotes.vue'
import ViewQuote from '../views/View-Booking.vue'
import NewQuote from '../views/Quotes/Create.vue'

import BuildQuotes from '../views/Build-Quotes.vue'
import ChooseTemplate from '../views/templates/Choose-Template.vue'
import TemplateList from '../views/templates/Template-list.vue'
import EmailList from '../views/templates/Email-list.vue'
import NewTemplate from '../views/templates/Create-Template.vue'

// Providers
import ProviderList from '../views/Providers/Provider-List.vue'
import NewProvider from '../views/Providers/New-Provider.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/quotes',
    name: 'Quotes',
    component: Quotes
  },
  {
    path: '/build-quotes',
    name: 'BuildQuotes',
    component: BuildQuotes
  },
  {
    path: '/view-quote/:id',
    name: 'ViewQuote',
    component: ViewQuote
  },
  {
    path: '/create-quote',
    name: 'NewQuote',
    component: NewQuote
  },
  {
    path: '/templates',
    name: 'ChooseTemplate',
    component: ChooseTemplate
  },
  {
    path: '/templates-list',
    name: 'TemplateList',
    component: TemplateList
  },
  {
    path: '/emails-list',
    name: 'EmailList',
    component: EmailList
  },
  {
    path: '/create-template',
    name: 'NewTemplate',
    component: NewTemplate
  },
  {
    path: '/new-provider',
    name: 'NewProvider',
    component: NewProvider
  },
  {
    path: '/providers',
    name: 'ProviderList',
    component: ProviderList
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
